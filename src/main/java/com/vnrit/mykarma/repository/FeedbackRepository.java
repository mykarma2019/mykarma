package com.vnrit.mykarma.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.vnrit.mykarma.model.Feedback;
import com.vnrit.mykarma.model.Order;

public interface FeedbackRepository extends JpaRepository<Feedback,Long>{
	
	@Query(value="select * from feedback where merchant_id=?1",nativeQuery=true)
	public Page<List<Map>> getReviewsOfMearchant(Long id,Pageable page);
	
	@Query(value="select * from feedback where user_profile_id=?1",nativeQuery=true)
	public Page<List<Map>> getReviewsOfUser(Long user_profile_id,Pageable page);
	
	@Query(value="select * from feedback where approve=true",nativeQuery=true)
	public Page<List<Map>> getApprovedTrue(Pageable page);
	
	@Query(value="select * from feedback where approve=false",nativeQuery=true)
	public Page<List<Map>> getApprovedFalse(Pageable page);
	
	@Modifying
	@Transactional
	@Query(value="update feedback set approve=true where id=?1",nativeQuery=true)
	public void updateApprovedTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update feedback set approve=false where id=?1",nativeQuery=true)
	public void updateApprovedFalse(Long id);
	
	@Modifying
	@Transactional
	@Query(value="delete from feedback where id=?1",nativeQuery=true)
	public void deleteReview(Long id);
}

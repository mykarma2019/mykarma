package com.vnrit.mykarma.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnrit.mykarma.model.Profile;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {
	
	@Query(value="select user_id from userprofile where id=?1",nativeQuery=true)
	public Long getUserId(Long id);
	
	@Modifying
	@Transactional
	@Query(value="delete from userprofile where id=?1",nativeQuery=true)
	public void deleteProfile(Long id);
	
	@Query(value="select users.created_at,users.api_token,users.otp,users.phone,users.approved,users.expiry_date,up.address,up.email,up.name,up.photo_path,up.user_id,users.id from users,userprofile up where users.id=up.user_id and approved=true",nativeQuery=true)
	public Page<List<Map>> getalldetailstrueusers(Pageable page);
	
	@Query(value="select users.created_at,users.api_token,users.otp,users.phone,users.approved,users.expiry_date,up.address,up.email,up.name,up.photo_path,up.user_id,users.id from users,userprofile up where users.id=up.user_id and approved=false",nativeQuery=true)
	public Page<List<Map>> getalldetailsfalseusers(Pageable page);
	
	@Query(value="select * from userprofile",nativeQuery=true)
	public Page<List<Map>> getallprofiles(Pageable page);

}
package com.vnrit.mykarma.repository;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnrit.mykarma.model.Items;


@Repository
public interface ItemsRepository extends JpaRepository<Items, Long>{
	
	@Query(value = "select * from items where id=?1", nativeQuery = true)
	  public  Items getItemDetailsById(Long id);
	
	@Query(value = "select profile_id from items where id=?1", nativeQuery = true)
	  public  Long getMerchantProfileId(Long id);
	
	@Query(value = "select quantity from items where id=?1", nativeQuery = true)
	  public  Long getQuantity(Long id);
	
	@Modifying
	@Transactional
	@Query(value = "delete from items where id=?1", nativeQuery = true)
	  public  void deleteItems(Long id);
	
	
	@Modifying
	@Transactional
	@Query(value = "update items set item_name=?1,item_type=?2,buy_before=?3,pickup_before=?4,original_price=?5,discount_price=?6,quantity=?7,rating=?8 where id=?9", nativeQuery = true)
	  public  void updateItemDetails(String itemName,String itemType,String buyBefore,String pickupBefore,int originalPrice,int discountPrice,int quantity,float rating,Long id);

}

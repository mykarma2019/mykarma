package com.vnrit.mykarma.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnrit.mykarma.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
	
	@Query(value="select * from orders where user_id=?1",nativeQuery=true)
	public List<Map> getOrderByUserId(Long user_id);
	
	@Modifying
	@Transactional
	@Query(value="delete from orders where id=?1",nativeQuery=true)
	public void deleteOrder(Long id);
	
	@Query(value="select user_id from orders where id=?1",nativeQuery=true)
	public Long getOrderUserId(Long id);
	
	@Query(value="select merchant_id from orders where id=?1",nativeQuery=true)
	public Long getOrderMerchantId(Long id);
	
	@Query(value="select * from orders where merchant_id=?1",nativeQuery=true)
	public Page<List<Map>> getOrdersOfMerchants(Long id,Pageable page);
	
	@Modifying
	@Transactional
	@Query(value="update orders set approved=true where id=?1",nativeQuery=true)
	public void updateApprovedTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update orders set approved=false where id=?1",nativeQuery=true)
	public void updateApprovedFalse(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update orders set completed=false where id=?1",nativeQuery=true)
	public void updateCompletedFalse(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update orders set completed=true where approved=true and id=?1",nativeQuery=true)
	public void updateCompletedTrue(Long id);
	
	@Query(value="select * from orders where merchant_id=?1 and approved=true",nativeQuery=true)
	public Page<List<Map>> getApprovedTrue(Long id,Pageable page);
	
	@Query(value="select * from orders where merchant_id=?1 and approved=false",nativeQuery=true)
	public Page<List<Map>> getApprovedFalse(Long id,Pageable page);
	
	@Query(value="select * from orders where merchant_id=?1 and completed=true",nativeQuery=true)
	public Page<List<Map>> getCompletedTrue(Long id,Pageable page);
	
	@Query(value="select * from orders where merchant_id=?1 and completed=false",nativeQuery=true)
	public Page<List<Map>> getCompletedFalse(Long id,Pageable page);
	
	@Query(value="select * from orders where user_id=?1 and approved=true",nativeQuery=true)
	public Page<List<Map>> getApprovedTrueOfUser(Long id,Pageable page);
	
	@Query(value="select * from orders where user_id=?1 and approved=false",nativeQuery=true)
	public Page<List<Map>> getApprovedFalseOfUser(Long id,Pageable page);
	
	@Query(value="select * from orders where user_id=?1 and completed=true",nativeQuery=true)
	public Page<List<Map>> getCompletedTrueOfUser(Long id,Pageable page);
	
	@Query(value="select * from orders where user_id=?1 and completed=false",nativeQuery=true)
	public Page<List<Map>> getCompletedFalseOfUser(Long id,Pageable page);
}

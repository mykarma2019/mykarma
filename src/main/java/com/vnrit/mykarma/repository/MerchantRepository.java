package com.vnrit.mykarma.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnrit.mykarma.model.Merchant;



@Repository
public interface MerchantRepository extends JpaRepository<Merchant, Long>{
	
	@Query(value = "select COUNT(*) from hotels where phone_number =?1", nativeQuery = true)
	  public  int findByphoneNumber(String number);
	
	@Query(value = "select otp from hotels where phone_number =?1", nativeQuery = true)
	  public  int findOtp(String phoneNumber);
	
	@Query(value = "select COUNT(*) from hotels where api_token=?1", nativeQuery = true)
	  public  int verifyApiToken(String apiToken);
	
	@Query(value = "select * from hotels where id=?1", nativeQuery = true)
	  public  Merchant getMerchantById(Long id);
	
	@Query(value = "select api_token from hotels where id=?1", nativeQuery = true)
	  public  String getdbApitoken(Long id);
	
	@Query(value = "select phone_number from hotels where id=?1", nativeQuery = true)
	  public  String getdbPhoneNumber(Long id);
	
	@Query(value = "select otp from hotels where id=?1", nativeQuery = true)
	  public  String getdbOtp(Long id);
	
	@Query(value = "select approved from hotels where id=?1", nativeQuery = true)
	  public  String getapproved(Long id);
	
	@Query(value = "select * from hotels where approved=true", nativeQuery = true)
	  public  List<Map> getapprovedTrueDetails(Boolean approved);
	
	@Query(value = "select id from hotels where phone_number=?1", nativeQuery = true)
	  public  Long getMerchantIdByNumber(String phoneNumber);
	
	@Query(value = "select * from hotels where approved=false", nativeQuery = true)
	  public List<Map>  getapprovedFalseDetails(Boolean approved);
	
	@Modifying
	@Transactional
	@Query(value = "delete  from hotels where id=?1", nativeQuery = true)
	  public  void deleteMerchant(Long id);
	
	@Modifying
	@Transactional
	@Query(value = "update hotels set approved=true where id=?1", nativeQuery = true)
	  public  void updateStatusTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value = "update hotels set approved=false where id=?1", nativeQuery = true)
	  public  void updateStatusFalse(Long id);
	
	@Modifying
	@Transactional
	@Query(value = "update hotels set api_token=?1, expiry_date=?2 where phone_number=?3", nativeQuery = true)
	  public  void updateApitoken(String apiToken,Date expiryDate,String phoneNumber);
	
	@Modifying
	@Transactional
	@Query(value = "update hotels set otp=?1 where phone_number=?2", nativeQuery = true)
	  public  void updateOtp(int otp,String phoneNumber);

}

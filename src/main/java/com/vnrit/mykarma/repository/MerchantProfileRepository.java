package com.vnrit.mykarma.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnrit.mykarma.model.MerchantProfile;
import com.vnrit.mykarma.model.TypeOfBussiness;

@Repository
public interface MerchantProfileRepository extends JpaRepository<MerchantProfile, Long>{
	
	@Query(value = "select * from profile where id=?1", nativeQuery = true)
	  public  MerchantProfile getMerchantProfileById(Long id);
	
	@Query(value = "select * from profile where approved=true", nativeQuery = true)
	  public List<Map> getAllStatusTrue(Boolean approved);
	
	@Query(value = "select * from profile where approved=false", nativeQuery = true)
	  public List<Map> getAllStatusFalse(Boolean approved);
	
	
	
	@Query(value = "select * from profile ", nativeQuery = true)
	  public List<Map> getAllProfile();
	
	@Query(value = "select merchant_id from profile where id=?1", nativeQuery = true)
	  public Long getMerchantId(Long id);
	
	@Modifying
	@Transactional
	@Query(value = "delete from profile where id=?1", nativeQuery = true)
	  public  void deleteMerchantProfile(Long id);
	
	
	
	@Modifying
	@Transactional
	@Query(value = "update profile set address=?1,approved=?2,category=?3,city=?4,state=?5,contact_person=?6,description=?7,hotel_name=?8 where id=?9", nativeQuery = true)
	  public  void updateMerchantProfileDetails(String address,Boolean approved,String category,String city,String state,String contactPerson,String description,String hotelName,Long id);

}

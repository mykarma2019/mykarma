package com.vnrit.mykarma.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.vnrit.mykarma.model.TypeOfBussiness;

@Repository
public interface TypeOfBussinessRepository extends JpaRepository<TypeOfBussiness, Long>{
	
	@Query(value = "select * from type_of_bussiness where id=?1", nativeQuery = true)
	  public  TypeOfBussiness getTypeById(Long id);
	
	@Query(value = "select * from type_of_bussiness where approved=true", nativeQuery = true)
	  public  List<Map> getAllStatusTrue(Boolean approved);
	
	@Query(value = "select * from type_of_bussiness where approved=false", nativeQuery = true)
	  public   List<Map> getAllStatusFalse(Boolean approved);
	
	@Modifying
	@Transactional
	@Query(value = "update type_of_bussiness set approved=true where id=?1", nativeQuery = true)
	  public  void updateBussinessTypeTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value = "delete from type_of_bussiness where id=?1", nativeQuery = true)
	  public  void deleteBussinessTypes(Long id);
	
	@Modifying
	@Transactional
	@Query(value = "update type_of_bussiness set approved=false where id=?1", nativeQuery = true)
	  public  void updateBussinessTypeFalse(Long id);

}

package com.vnrit.mykarma.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vnrit.mykarma.model.Admin;


public interface AdminRepository extends JpaRepository<Admin, Long> {
	
	@Query(value = "select id from admins where email=?1 and password=?2", nativeQuery = true)
	public Long  getAdminIdByEmailPwd(String emailAddress,String pwd);
	
	@Query(value = "select COUNT(*) from admins where email =?1 and password=?2", nativeQuery = true)
	  public  int  findByEmailAddressPwd (String emailAddress,String pwd);
	
	@Query(value = "select count(*) from admins where api_token=?1", nativeQuery = true)
	public int verifyapiTokens(String headerToken);


}

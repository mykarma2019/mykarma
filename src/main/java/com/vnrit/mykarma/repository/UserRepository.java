package com.vnrit.mykarma.repository;

import java.time.LocalDate;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnrit.mykarma.model.User;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
	
	@Query(value="select COUNT(*) from users where phone=?1",nativeQuery = true)
	public int findPhone(String phone);
	
	@Query(value="select COUNT(*) from users where otp=?1",nativeQuery = true)
	public int findOtp(int otp);
	
	@Query(value="select id from users where phone=?1",nativeQuery = true)
	public Long findId(Long id);
	
	@Query(value="select id from users where phone=?1",nativeQuery = true)
	public Long findId1(String id);
	
	@Query(value="select otp from users where phone=?1",nativeQuery = true)
	public int findOtpByPhone(String phone);
	
	@Modifying
	@Transactional
	@Query(value="update users set api_token=?1,expiry_date=?2 where phone=?3",nativeQuery=true)
	public void updateToken(String apiToken,LocalDate expiryDate,String phone);
	
	@Modifying
	@Transactional
	@Query(value="update users set api_token=?1,expiry_date=?2,otp=?3 where phone=?4",nativeQuery=true)
	public void updateTokenOtp(String apiToken,LocalDate expiryDate,int otp,String phone);

	@Modifying
	@Transactional
	@Query(value="update users set otp=?1 where phone=?2",nativeQuery=true)
	public void updateOtp(int otp,String phone);
	
	@Query(value="select expiry_date from users where phone=?1",nativeQuery=true)
	public String expiryDate(String phone);
	
	@Query(value="select api_token from users where id=?1",nativeQuery=true)
	public String getDbApiToken(long userid);
	
	@Query(value = "select count(*) from users where api_token=?1", nativeQuery = true)
	public int verifyUserApiToken(String headerToken);
	
	@Modifying
	@Transactional
	@Query(value="delete from users where id=?1",nativeQuery=true)
	public void deleteUser(Long id);
	
	@Query(value="select * from users where approved=true",nativeQuery=true)
	public String getApprovedUsers();
	
	@Query(value="select * from users where approved=false",nativeQuery=true)
	public String getUnApprovedUsers();
	
	@Modifying
	@Transactional
	@Query(value="update users set approved=true where id=?1",nativeQuery=true)
	public void updateApproveTrue(long id);
	
	@Modifying
	@Transactional
	@Query(value="update users set approved=false where id=?1",nativeQuery=true)
	public void updateApproveFalse(long id);

}

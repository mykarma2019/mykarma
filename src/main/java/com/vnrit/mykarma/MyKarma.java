package com.vnrit.mykarma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


/**
 * Hello world!
 *
 */

@EnableJpaAuditing
@SpringBootApplication
public class MyKarma
{
    public static void main( String[] args )
    {
    	SpringApplication.run(MyKarma.class, args);
    }
}

package com.vnrit.mykarma.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnauthorisedException extends RuntimeException {

	
	private int code;
	private String error;
	private String message;


	public  UnauthorisedException(int code, String error, String message) {
		super();
		this.code = code;
		this.message = message;
		this.error = error;
	}
	
	
	public  UnauthorisedException() {
		super();

	}

	public UnauthorisedException(String message) {
		super(message);
	}

	public UnauthorisedException(String message, Throwable cause) {
		super(message, cause);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}



}

package com.vnrit.mykarma.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.vnrit.mykarma.exception.UnauthorisedException;
import com.vnrit.mykarma.repository.AdminRepository;
import com.vnrit.mykarma.repository.MerchantRepository;
import com.vnrit.mykarma.model.Merchant;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class MerchantController {
	
	@Value("${expiry-date}")
	private int expiry;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private MerchantRepository merchantRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@PostMapping("/api/createmerchantbynumber")
	public JSONObject postMerchant(@RequestBody Merchant merchant) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
	String phoneNumber = merchant.getPhoneNumber();
	
	
	int result = merchantRepository.findByphoneNumber(phoneNumber);
	 
	     if(result==0) {
	    	 
	    	 Random random=new Random();
	            int otp = 100000+ random.nextInt(900000);
	            
	            merchant.setOtp(otp);
//	            write sms code
	            
	          merchantRepository.save(merchant);
	          statusObject.put("code", 200);
			statusObject.put("message", "successfull");
			contentObject.put("phoneNumber",merchant.getPhoneNumber() );
			contentObject.put("otp", otp);
			
			jsonObject.put("content", contentObject);
			jsonObject.put("status", statusObject);
	         
	            
	    	 
	     }
	     else if(result==1) {
	    	 
	    	 Random random=new Random();
	            int otp = 100000+ random.nextInt(900000);
	            
	            merchant.setOtp(otp);
//	            write sms code
	            merchantRepository.updateOtp(otp, phoneNumber);
	            statusObject.put("code", 200);
				statusObject.put("message", "successfully updated otp");
				contentObject.put("phoneNumber",merchant.getPhoneNumber() );
				contentObject.put("otp", otp);
				
				jsonObject.put("content", contentObject);
				jsonObject.put("status", statusObject);
	    	 
	     }
		return jsonObject;
		
		
	 
		
	}
	
	@PostMapping("/api/sendapi")
	public JSONObject saveApitoken(@RequestBody  Merchant merchant)
	{
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String phoneNumber = merchant.getPhoneNumber();
		
		
		int result = merchantRepository.findByphoneNumber(phoneNumber);
		
		if(result==1) {
			int otp = merchant.getOtp();
			
			int findOtp = merchantRepository.findOtp(phoneNumber);
			
			if(findOtp==otp) {
				Long merchantIdByNumber = merchantRepository.getMerchantIdByNumber(phoneNumber);
				Merchant merchantObj = merchantRepository.getOne(merchantIdByNumber);
				Date expiryDate = merchant.getExpiryDate();
				
//				Date date = new Date();
//				Calendar calen1 = Calendar.getInstance(); 
//		        Calendar calen2 = Calendar.getInstance(); 
//		        calen1.setTime(expiryDate); 
//		        calen2.setTime(date);
				
				
					
					
					
					UUID apiToken = UUID.randomUUID();
					
					merchantObj.setApiToken(apiToken.toString());
					
//					Date date=new Date();
					Calendar c = Calendar.getInstance();
					c.add(Calendar.DATE,expiry);  
					Date d=c.getTime();
					merchantObj.setExpiryDate(d);
					merchantObj.setApproved(true);
					
					merchantRepository.save(merchantObj);
					
//					merchantRepository.updateApitoken(merchant.getApiToken(), merchant.getExpiryDate(), phoneNumber);
					 statusObject.put("code", 200);
						statusObject.put("message", "successfully updated apitoken and expirydate");
						contentObject.put("apiToken",merchantObj.getApiToken() );
						contentObject.put("otp", otp);
						
						jsonObject.put("content", contentObject);
						jsonObject.put("status", statusObject);
					
				
				
				
				
				
			}
			else {
				 statusObject.put("code", 400);
					statusObject.put("message", "otp is not correct");
					jsonObject.put("status", statusObject);
				
			}
			
		}else {
			 statusObject.put("code", 400);
				statusObject.put("message", "phonenumber is not found");
				jsonObject.put("status", statusObject);
		}
		
		
		return jsonObject;
	}
	
	@GetMapping("/api/getmerchantdetails")
	public List<Merchant> getAllHotelMerchants(Merchant merchant) {
		
		Long id = merchant.getId();
        
		String headerToken = request.getHeader("apiToken");
		
		 int verifyApiToken = adminRepository.verifyapiTokens(headerToken);

		

		if (verifyApiToken==0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
	return	merchantRepository.findAll();
		
	}
	
	@GetMapping("/api/getmerchantbyid/{id}")
	public JSONObject getMerchantById(@PathVariable(value = "id") Long id, Merchant merchant) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		
		
		String dbApiToken = merchantRepository.getdbApitoken(id);
		
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
		 Merchant merchantById = merchantRepository.getMerchantById(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "successfull");
		
		contentObject.put("content", merchantById);
		
		jsonObject.put("content", contentObject);
		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@GetMapping("/api/getmerchantbyidadmin/{id}")
	public JSONObject getMerchantByIdAdmin(@PathVariable(value = "id") Long id, Merchant merchant) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		
		
		 int verifyApiToken = adminRepository.verifyapiTokens(headerToken);

			

			if (verifyApiToken==0) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			 Merchant merchantById = merchantRepository.getMerchantById(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "successfull");
		contentObject.put("content",merchantById );
		
		
		
		jsonObject.put("content", contentObject);
		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@GetMapping("/api/getallstatustruemerchantdetails/{status}")
	public List<Map> getAllStatusTrueDetails(@PathVariable(value = "status") Boolean status,Merchant merchant) {
		
		Long id = merchant.getId();
		
		String headerToken = request.getHeader("apiToken");
		int verifyapiTokens = adminRepository.verifyapiTokens(headerToken);
		if(verifyapiTokens==0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
	return	merchantRepository.getapprovedTrueDetails(status);
		
		
	}
	
	@GetMapping("/api/getallstatusfalsemerchantdetails/{status}")
	public List<Map> getAllStatusFalseDetails(@PathVariable(value = "status") Boolean status,Merchant merchant) {
		
		Long id = merchant.getId();
		
		String headerToken = request.getHeader("apiToken");
		int verifyapiTokens = adminRepository.verifyapiTokens(headerToken);
		if(verifyapiTokens==0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
	return	merchantRepository.getapprovedFalseDetails(status);
		
		
	}
	
	@PatchMapping("/api/updatestatustrue/{id}")
	public JSONObject updatestatusTrue(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiTokens = adminRepository.verifyapiTokens(headerToken);
          if(verifyapiTokens==0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
		
		merchantRepository.updateStatusTrue(id);
		statusObject.put("code", 200);
		statusObject.put("message", "successfully updated status");
		
		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	@PatchMapping("/api/updatestatusfalse/{id}")
	public JSONObject updatestatusFalse(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiTokens = adminRepository.verifyapiTokens(headerToken);
        if(verifyapiTokens==0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
		
		merchantRepository.updateStatusFalse(id);
		statusObject.put("code", 200);
		statusObject.put("message", "successfully updated status");
		
		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	@PatchMapping("/api/updatestatusfalsebymerchant/{id}")
	public JSONObject updatestatusFalseBymerchant(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String headerToken = request.getHeader("apiToken");
		
		String getdbApitoken = merchantRepository.getdbApitoken(id);
        if(!getdbApitoken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
		
		merchantRepository.updateStatusFalse(id);
		statusObject.put("code", 200);
		statusObject.put("message", "successfully updated status");
		
		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	@DeleteMapping("/api/deletemerchant/{id}")
	public JSONObject deleteMerchant(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String headerToken = request.getHeader("apiToken");
		
		String dbApiToken = merchantRepository.getdbApitoken(id);
		

        if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
        merchantRepository.deleteMerchant(id);
        statusObject.put("code", 200);
		statusObject.put("message", "successfully deleted");
		
		jsonObject.put("status", statusObject);
		
		return jsonObject;
		
		
		
		
	}
	@DeleteMapping("/api/deletemerchantbyadmin/{id}")
	public JSONObject deleteMerchantByAdmin(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiTokens = adminRepository.verifyapiTokens(headerToken);
        if(verifyapiTokens==0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
        merchantRepository.deleteMerchant(id);
        statusObject.put("code", 200);
		statusObject.put("message", "successfully deleted");
		
		jsonObject.put("status", statusObject);
		
		return jsonObject;
		
		
		
		
	}

}

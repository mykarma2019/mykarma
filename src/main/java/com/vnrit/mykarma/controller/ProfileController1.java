package com.vnrit.mykarma.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnrit.mykarma.exception.ResourceNotFoundException;
import com.vnrit.mykarma.exception.UnauthorisedException;
import com.vnrit.mykarma.model.Profile;
import com.vnrit.mykarma.repository.AdminRepository;
import com.vnrit.mykarma.repository.ProfileRepository;
import com.vnrit.mykarma.repository.UserRepository;


@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class ProfileController1 {
	
	@Value("${profile.upload-dir}")
	private String uploadPath;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@PostMapping("/api/postprofile/{userId}")
	public JSONObject createProfile(@PathVariable(value="userId")Long userId,@Valid @ModelAttribute Profile profile,@RequestParam MultipartFile file) {
	
	JSONObject jsonObject = new JSONObject();
	JSONObject contentObject = new JSONObject();
	JSONObject statusObject = new JSONObject();
	
	String headerToken = request.getHeader("apiToken");
	
//	long getUserId=profileRepository.getUserId(userId);
	
	String dbApiToken = userRepository.getDbApiToken(userId);
	
	if(!dbApiToken.equals(headerToken)) {
	
	String error = "UnAuthorised User";
	String message = "Not Successful";
	
	throw new UnauthorisedException(401, error, message);
}
	
	String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
	
	String path=uploadPath+fileName;
	profile.setPhotoPath(path);
	
	OutputStream opStream = null;
	
	byte[] byteContent;
	try {
		byteContent = file.getBytes();
		File myFile = new File(path); // destination path
		System.out.println("fileName is "+myFile);
		
		// check if file exist, otherwise create the file before writing
		if (!myFile.exists()) {
			
			myFile.createNewFile();
			
		}
		
		opStream = new FileOutputStream(myFile);
		opStream.write(byteContent);
		opStream.flush();
		opStream.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	
	profileRepository.save(profile);
	
	statusObject.put("code", 200);
	statusObject.put("message", "Profile updated successfully");
	contentObject.put("ImagePath", path);

	jsonObject.put("status", statusObject);
	return jsonObject;
}
	

	
	@GetMapping("/api/getprofile/{id}")
	public Profile getProfile(@PathVariable(value="id")Long userId) {
		
		String headerToken = request.getHeader("apiToken");
		
		long getUserId=profileRepository.getUserId(userId);
		
		String dbApiToken = userRepository.getDbApiToken(getUserId);
		
		if(!dbApiToken.equals(headerToken)) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";
		
		throw new UnauthorisedException(401, error, message);
	}
		
		Profile profile = profileRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("id", "this", userId));
		return profile;
	}
	
	@PatchMapping("/api/updateProfile/{id}")
	public JSONObject updateProfile(@PathVariable(value="id")long id,@RequestBody Profile profile) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long getUserId=profileRepository.getUserId(id);
		
		String dbApiToken = userRepository.getDbApiToken(getUserId);
		
		if(!dbApiToken.equals(headerToken)) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";
		
		throw new UnauthorisedException(401, error, message);
	}
		
		String name=profile.getName();
		
		String address=profile.getAddress();
		
		String email=profile.getEmail();
		
		Profile dbObj = profileRepository.getOne(id);
		
		if(name != null) {
			dbObj.setName(name);
		}
		
		if(address != null) {
			dbObj.setAddress(address);
		}
		
		if(email != null) {
			dbObj.setEmail(email);
		}
		
		profileRepository.save(dbObj);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Profile updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
	}
	
	@DeleteMapping("/api/deleteprofile/{id}")
	public JSONObject deleteProfile(@PathVariable(value="id")Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long getUserId=profileRepository.getUserId(id);
		
		String dbApiToken = userRepository.getDbApiToken(getUserId);
		
		if(!dbApiToken.equals(headerToken)) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";
		
		throw new UnauthorisedException(401, error, message);
	}
		
		profileRepository.deleteProfile(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Profile deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@DeleteMapping("/api/admindeleteprofile/{id}")
	public JSONObject deleteProfileByAdmin(@PathVariable(value="id")Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		profileRepository.deleteProfile(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Profile deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@GetMapping("/api/getalluserwithprofile")
	public Page<List<Map>> getUserWithProfile(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return profileRepository.getalldetailstrueusers(page);
	}

	@GetMapping("/api/getallfalseuserwithprofile")
	public Page<List<Map>> getFalseUserWithProfile(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return profileRepository.getalldetailsfalseusers(page);
	}


	@GetMapping("/api/getalluserprofiles")
	public Page<List<Map>> getAllUsersProfiles(Pageable page)
	{
		
        String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return profileRepository.getallprofiles(page);
	}
	
}

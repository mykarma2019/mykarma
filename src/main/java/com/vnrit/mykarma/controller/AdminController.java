package com.vnrit.mykarma.controller;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnrit.mykarma.exception.UnauthorisedException;
import com.vnrit.mykarma.model.Admin;
import com.vnrit.mykarma.model.Profile;
import com.vnrit.mykarma.model.User;
import com.vnrit.mykarma.repository.AdminRepository;
import com.vnrit.mykarma.repository.ProfileRepository;
import com.vnrit.mykarma.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AdminController {
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private UserRepository userRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
	
	@PostMapping("/api/postadmin")
	public JSONObject postAdmin(@RequestBody Admin admin) {
		 
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String email = admin.getEmail();
		String password = admin.getPassword();

		
		UUID apiToken = UUID.randomUUID();
		
		admin.setApiToken(apiToken.toString());
		
		//create expiry date
		
		Date date=new Date();
		admin.setExpiryDate(date);

		
		int id = adminRepository.findByEmailAddressPwd(email,password);
		statusObject.put("code", 200);
		statusObject.put("message", "user registered successfully");
		contentObject.put("content", adminRepository.save(admin));
		contentObject.put("apiToken", admin.getApiToken());
		
		jsonObject.put("content", contentObject);
		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@PostMapping("/api/post/signinadmin")
	public JSONObject createsignin(@Valid @RequestBody Admin signin) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String email = signin.getEmail();
		String pwd = signin.getPassword();

		if(email != null && pwd !=null) {
			logger.info("email is present :"+email);
			int result = adminRepository.findByEmailAddressPwd(email, pwd);
		
		if (result == 0) {
					
			statusObject.put("code", 401);
			statusObject.put("message", "please check credentials & try again!");
			
			jsonObject.put("status", statusObject);
			
			return jsonObject;

		}

		else {

			int id = adminRepository.findByEmailAddressPwd(email,pwd);
			Admin adminObj = adminRepository.getOne((long) id);
			
			statusObject.put("code", 200);
			statusObject.put("message", "signin successful");
			contentObject.put("apiToken", adminObj.getApiToken());
			contentObject.put("id", id);
			
			
			jsonObject.put("content", contentObject);
			jsonObject.put("status", statusObject);
			
			return jsonObject;
			
		}
			

		}
		statusObject.put("code", 400);
		statusObject.put("message", "please enter emailid and password ");
		
		
		jsonObject.put("content", contentObject);
		jsonObject.put("status", statusObject);
		
return jsonObject;
	}

	
	@GetMapping("/api/getalluser")
	public Page<User> getAllUsers(Pageable page)
	{
		
        String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return userRepository.findAll(page);
	}
	
	@GetMapping("/api/getapprovedusers")
	public String getApprovedUsers()
	{
		
        String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return userRepository.getApprovedUsers();
	}
	
	@GetMapping("/api/getunapprovedusers")
	public String getUnApprovedUsers()
	{
		
        String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return userRepository.getUnApprovedUsers();
	}
	
	@DeleteMapping("/api/deleteusers/{id}")
	public JSONObject deleteUser(@PathVariable(value="id")Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		 String headerToken = request.getHeader("apiToken");
			
	        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
			
			if(verifyapiToken == 0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";
				
				throw new UnauthorisedException(401, error, message);
			}
			
			 userRepository.deleteUser(id);
			 
			 statusObject.put("code", 400);
				statusObject.put("message", "User Deleted ");
				
				jsonObject.put("status", statusObject);
				
				return jsonObject;
	}
	
	@PatchMapping("/api/updateUserTrue")
	public JSONObject updateUserTrue(@PathVariable(value="id")Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		userRepository.updateApproveTrue(id);
		
		 statusObject.put("code", 400);
			statusObject.put("message", "Updated user approved true ");
			
			jsonObject.put("status", statusObject);
			
			return jsonObject;
	}
	
	@PatchMapping("/api/updateUserFalse")
	public JSONObject updateUserFalse(@PathVariable(value="id")Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		userRepository.updateApproveFalse(id);
		
		 statusObject.put("code", 400);
			statusObject.put("message", "Updated user approved false ");
			
			jsonObject.put("status", statusObject);
			
			return jsonObject;
	}
	
	@GetMapping("/api/getallprofiles")
	public List<Profile> getAllProfiles(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return profileRepository.findAll();
	}


}

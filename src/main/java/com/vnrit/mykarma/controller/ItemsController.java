package com.vnrit.mykarma.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnrit.mykarma.repository.AdminRepository;
import com.vnrit.mykarma.repository.ItemsRepository;
import com.vnrit.mykarma.repository.MerchantProfileRepository;
import com.vnrit.mykarma.repository.MerchantRepository;
import com.vnrit.mykarma.exception.UnauthorisedException;
import com.vnrit.mykarma.model.Items;
import com.vnrit.mykarma.model.Merchant;
import com.vnrit.mykarma.model.MerchantProfile;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;



@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ItemsController {
	
	
	@Autowired
	private ItemsRepository itemsRepository;
	
	@Value("${file.uploaditems-dir}")
	private String uploadPath;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private MerchantRepository merchantRepository;
	
	@Autowired
	private MerchantProfileRepository merchantProfileRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@PostMapping("/api/saveitemdetailsbymerchantid/{profileId}")
	public JSONObject postItems(@PathVariable(value = "profileId") Long profileId,@ModelAttribute Items item,@RequestParam MultipartFile file) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		
//		Long merchantProfileId = itemsRepository.getMerchantProfileId(profileId);
		Long merchantId = merchantProfileRepository.getMerchantId(profileId);
		String dbApiToken = merchantRepository.getdbApitoken(merchantId);
		
          if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
          String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
          String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");
          item.setFileName(fileName);
          OutputStream opStream = null;
          
          byte[] byteContent;
  		try {
  			byteContent = file.getBytes();
  			File myFile = new File(uploadPath + fileName); // destination path
  			System.out.println("fileName is "+myFile);
  			
  			
  			
  			// check if file exist, otherwise create the file before writing
  			if (!myFile.exists()) {
  				
  				myFile.createNewFile();
  				
  			}
  			
  			opStream = new FileOutputStream(myFile);
  			opStream.write(byteContent);
  			opStream.flush();
  			opStream.close();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		try {
  			
  			File destinationDir = new File(uploadPath);

  			Thumbnails.of(uploadPath +fileName)
  			        .size(900,800)
  			        .toFiles(destinationDir, Rename.NO_CHANGE);
  			
  			Thumbnails.of(new File(uploadPath + fileName))
  			.size(348, 235)
  			.toFile(uploadPath +thumbnailName);
  			
  			
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		MerchantProfile merchantProfileObj = merchantProfileRepository.getOne(profileId);
  		

  		item.setProfileId(merchantProfileObj);
		
		Items save = itemsRepository.save(item);
		statusObject.put("code", 200);
		statusObject.put("message", "successfull");
		contentObject.put("object", save);
		jsonObject.put("content", contentObject);
        jsonObject.put("status", statusObject);
		
		return jsonObject;
		
	}
	
	@PostMapping("/api/saveitemdetailsbymerchantidadmin/{profileId}")
	public JSONObject postItemsByAdmin(@PathVariable(value = "profileId") Long profileId,@ModelAttribute Items item,@RequestParam MultipartFile file) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
	     int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
	     
		if(verifyApiToken==0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
          String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
          String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");
          item.setFileName(fileName);
          OutputStream opStream = null;
          
          byte[] byteContent;
  		try {
  			byteContent = file.getBytes();
  			File myFile = new File(uploadPath + fileName); // destination path
  			System.out.println("fileName is "+myFile);
  			
  			
  			
  			// check if file exist, otherwise create the file before writing
  			if (!myFile.exists()) {
  				
  				myFile.createNewFile();
  				
  			}
  			
  			opStream = new FileOutputStream(myFile);
  			opStream.write(byteContent);
  			opStream.flush();
  			opStream.close();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		try {
  			
  			File destinationDir = new File(uploadPath);

  			Thumbnails.of(uploadPath +fileName)
  			        .size(900,800)
  			        .toFiles(destinationDir, Rename.NO_CHANGE);
  			
  			Thumbnails.of(new File(uploadPath + fileName))
  			.size(348, 235)
  			.toFile(uploadPath +thumbnailName);
  			
  			
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		MerchantProfile merchantProfileById = merchantProfileRepository.getMerchantProfileById(profileId);
  		
  		item.setProfileId(merchantProfileById);
		
		Items save = itemsRepository.save(item);
		statusObject.put("code", 200);
		statusObject.put("message", "successfull");
		contentObject.put("object", save);
		jsonObject.put("content", contentObject);
        jsonObject.put("status", statusObject);
		
		return jsonObject;
		
	}
	
	@GetMapping("/api/getallitems")
	public List<Items> getAllItems() {
		
		
         String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return itemsRepository.findAll();
	}
	
	@GetMapping("/api/getitemdetailsbyid/{id}")
	public Items getItemsDetails(@PathVariable(value = "id") Long id,Items item) {
		
		 String headerToken = request.getHeader("apiToken");
		 
		 
		
		 Long merchantProfileId = itemsRepository.getMerchantProfileId(id);
			Long merchantId = merchantProfileRepository.getMerchantId(merchantProfileId);
			String dbApiToken = merchantRepository.getdbApitoken(merchantId);
		 
		 if(!dbApiToken.equals(headerToken)) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return itemsRepository.getItemDetailsById(id);
		
	}
	
	@GetMapping("/api/getitemdetailsbyidadmin/{id}")
	public Items getItemsDetailsByAdmin(@PathVariable(value = "id") Long id) {
		
		 String headerToken = request.getHeader("apiToken");
		 
		 int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return itemsRepository.getItemDetailsById(id);
		
	}
	
	@PatchMapping("/api/updateitemdetails/{id}")
	public JSONObject updateItemDetails(@PathVariable(value = "id") Long id,@RequestBody Items item) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		Long merchantProfileId = itemsRepository.getMerchantProfileId(id);
		String getdbApitoken = merchantRepository.getdbApitoken(merchantProfileId);
		
		if(!getdbApitoken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
		
		Items one = itemsRepository.getOne(id);
		int discountPrice = item.getDiscountPrice();
		String itemName = item.getItemName();
		String itemType = item.getItemType();
		int originalPrice = item.getOriginalPrice();
		int quantity = item.getQuantity();
		float rating = item.getRating();
		String buyBefore = item.getBuyBefore();
		String pickupBefore = item.getPickupBefore();
		
		if(itemName == null) {
			itemName=one.getItemName();
		}
		if(itemType==null) {
			itemType=one.getItemType();
		}
		if(originalPrice==0) {
			originalPrice=one.getOriginalPrice();
		}
		if(discountPrice==0) {
			discountPrice=one.getDiscountPrice();
		}
		if(quantity==0) {
			quantity=one.getQuantity();
		}
		if(rating==0.0f) {
			rating=one.getRating();
		}
		if(buyBefore==null) {
			buyBefore=one.getBuyBefore();
		}
		if(pickupBefore==null) {
			pickupBefore=one.getPickupBefore();
		}
		itemsRepository.updateItemDetails(itemName, itemType, buyBefore, pickupBefore, originalPrice, discountPrice, quantity, rating, id);
		
		
		 statusObject.put("code", 200);
			statusObject.put("message", "updated successfull");
		
			jsonObject.put("status", statusObject);
			return  jsonObject;
	}
	
	@PatchMapping("/api/updateitemdetailsbyadmin/{id}")
	public JSONObject updateItemDetailsByAdmin(@PathVariable(value = "id") Long id,@RequestBody Items item) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		
		Items one = itemsRepository.getOne(id);
		int discountPrice = item.getDiscountPrice();
		String itemName = item.getItemName();
		String itemType = item.getItemType();
		int originalPrice = item.getOriginalPrice();
		int quantity = item.getQuantity();
		float rating = item.getRating();
		String buyBefore = item.getBuyBefore();
		String pickupBefore = item.getPickupBefore();
		
		if(itemName == null) {
			itemName=one.getItemName();
		}
		if(itemType==null) {
			itemType=one.getItemType();
		}
		if(originalPrice==0) {
			originalPrice=one.getOriginalPrice();
		}
		if(discountPrice==0) {
			discountPrice=one.getDiscountPrice();
		}
		if(quantity==0) {
			quantity=one.getQuantity();
		}
		if(rating==0.0f) {
			rating=one.getRating();
		}
		if(buyBefore==null) {
			buyBefore=one.getBuyBefore();
		}
		if(pickupBefore==null) {
			pickupBefore=one.getPickupBefore();
		}
		itemsRepository.updateItemDetails(itemName, itemType, buyBefore, pickupBefore, originalPrice, discountPrice, quantity, rating, id);
		
		
		 statusObject.put("code", 200);
			statusObject.put("message", "updated successfull");
		
			jsonObject.put("status", statusObject);
			return  jsonObject;
	}
	
	@DeleteMapping("/api/deleteitems/{id}")
	public JSONObject deleteItems(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		 Long merchantProfileId = itemsRepository.getMerchantProfileId(id);
			Long merchantId = merchantProfileRepository.getMerchantId(merchantProfileId);
			String dbApiToken = merchantRepository.getdbApitoken(merchantId);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
		itemsRepository.deleteItems(id);
		 statusObject.put("code", 200);
			statusObject.put("message", "successfully deleted");
			
			jsonObject.put("status", statusObject);
			
			return jsonObject;
		
	}
	
	@DeleteMapping("/api/deleteitems/admin/{id}")
	public JSONObject deleteItemsByAdmin(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		itemsRepository.deleteItems(id);
		 statusObject.put("code", 200);
			statusObject.put("message", "successfully deleted");
			
			jsonObject.put("status", statusObject);
			
			return jsonObject;
		
	}

}

package com.vnrit.mykarma.controller;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnrit.mykarma.repository.TypeOfBussinessRepository;
import com.vnrit.mykarma.model.TypeOfBussiness;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class TypeOfBussinessController {
	
	@Autowired
	private TypeOfBussinessRepository typeOfBussinessRepository;
	
	
	@PostMapping("/api/posttypeofbussiness")
	public TypeOfBussiness postTypeOfBussiness(@RequestBody TypeOfBussiness bussiness) {
		
		return typeOfBussinessRepository.save(bussiness);
		
	}
	
	
	@GetMapping("/api/getallbussinesstypes")
	public List<TypeOfBussiness> getAllTypes() {
		
	return	typeOfBussinessRepository.findAll();
		
	}
	
	
	@GetMapping("/api/getbussinesstypebyid/{id}")
	public TypeOfBussiness getBussinessTypeById(@PathVariable(value="id")Long id) {
		
	return	typeOfBussinessRepository.getTypeById(id);
		
	}
	
	@PatchMapping("/api/updatebussinessstatustotrue/{id}")
	public JSONObject updateStatusTrue(@PathVariable(value="id")Long id,@RequestBody TypeOfBussiness bussiness) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		typeOfBussinessRepository.updateBussinessTypeTrue(id);
		 statusObject.put("code", 200);
			statusObject.put("message", "successfully updates");
			
			jsonObject.put("status", statusObject);
			
			return jsonObject;
		
	}
	
	@PatchMapping("/api/updatebussinessstatustofalse/{id}")
	public JSONObject updateStatusFalse(@PathVariable(value="id")Long id,@RequestBody TypeOfBussiness bussiness) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		typeOfBussinessRepository.updateBussinessTypeFalse(id);
		statusObject.put("code", 200);
		statusObject.put("message", "successfully updates");
		
		jsonObject.put("status", statusObject);
		
		return jsonObject;
		
	}
	
	@GetMapping("/api/getallbussinesstypetrue/{status}")
	public List<Map> getAllBussinesstypeTrue(@PathVariable(value="status")Boolean status) {
		
		return typeOfBussinessRepository.getAllStatusTrue(status);
	}
	
	@GetMapping("/api/getallbussinesstypefalse/{status}")
	public List<Map> getAllBussinesstypeFalse(@PathVariable(value="status")Boolean status) {
		
		return typeOfBussinessRepository.getAllStatusFalse(status);
	}
	
	@DeleteMapping("/api/deletebussinesstypes/{id}")
	public JSONObject deleteBussinessTypes(@PathVariable(value="id")Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		typeOfBussinessRepository.deleteBussinessTypes(id);
		statusObject.put("code", 200);
		statusObject.put("message", "successfully deleted");
		
		jsonObject.put("status", statusObject);
		
		return jsonObject;
		
		
	}

}

package com.vnrit.mykarma.controller;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vnrit.mykarma.exception.UnauthorisedException;
import com.vnrit.mykarma.model.User;
import com.vnrit.mykarma.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {
	
	@Value("${expiry-date}")
	private int expiry;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private UserRepository userRepository;
	
	
	
	@PostMapping("/api/generateotp")
	public  JSONObject saveUser(@Valid @RequestBody User user)
	{
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String phone=user.getPhone();
		Date expiryDate= user.getExpiryDate();
		
			int result = userRepository.findPhone(phone);
			if(result == 0) {
				
				Random random=new Random();
				int otp = 100000+ random.nextInt(900000);
				user.setOtp(otp);
				
				// Write Sms Code		
				
				userRepository.save(user);

				statusObject.put("code", 200);
				statusObject.put("message", "successfull");
				
				jsonObject.put("status", statusObject);
				return jsonObject;
				
				}
//			else {
//				String expirydate=userRepository.expiryDate(phone);
//				Random random=new Random();
//				int otp = 100000+ random.nextInt(900000);
//				user.setOtp(otp);
//				
//				// Write Sms Code		
//				  
//				  String pattern = "yyyy-MM-dd HH:mm:ss";
//				  SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//
//				  String date = simpleDateFormat.format(new Date());
//				  System.out.println(date);
//				  String date1=userRepository.expiryDate(phone);
//				System.out.println(date1);
//				System.out.println(date);
//				
//				
//				if(date == date1) {
//					
//					UUID apiToken = UUID.randomUUID();
//					
//					user.setApiToken(apiToken.toString());
//					
////					Calendar c = Calendar.getInstance();
////			        c.add(Calendar.DATE, expiry);
////					Date today = Calendar.getInstance().getTime();
////					String pattern1 = "yyyy-MM-dd";
////					  SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);
////					  
////					  String folderName = simpleDateFormat1.format(today);
////
////					  String date2 = simpleDateFormat1.format(new Date());
////					  today.add(SimpleDateFormat.DATE_FIELD,expiry);
////			        Date d = c.getTime();
////			        System.out.println(d);
//			        
//			        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
//			    	LocalDate localDate = LocalDate.now();
//			    	String time=dtf.format(localDate);
////			    	System.out.println(dtf.format(localDate));
//			    	LocalDate tomorrow = localDate.plusDays(expiry); 
//			    	System.out.println(tomorrow);
//			        
//			        userRepository.updateTokenOtp(user.getApiToken(), tomorrow,user.getOtp(), phone);
//					
//				}
	else {
		Random random=new Random();
		int otp = 100000+ random.nextInt(900000);
		user.setOtp(otp);
		
		// Write Sms Code
		
				userRepository.updateOtp(user.getOtp(), phone);
				
				statusObject.put("code", 200);
				statusObject.put("message", "successfull");
				
				jsonObject.put("status", statusObject);
				return jsonObject;
				}
				
		}

	
	@PostMapping("/api/verifyOtp")
	public JSONObject saveApi(@RequestBody User user) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		int Otp =user.getOtp();
		String phone=user.getPhone();
		
		int result=userRepository.findPhone(phone);

		System.out.println(Otp);
		if(result==1) {
		int dbOtp=userRepository.findOtpByPhone(phone);
		if(Otp==dbOtp) {
			UUID apiToken = UUID.randomUUID();
			
			user.setApiToken(apiToken.toString());
			
			//create expiry date
			
//			Date date=new Date(); // +30 days
//			user.setExpiryDate(date+expiry);
			
//			Calendar c = Calendar.getInstance();
//			c.set(Calendar.HOUR_OF_DAY, 0);
//			c.set(Calendar.MINUTE, 0);
//			c.set(Calendar.SECOND, 0);
//	        c.add(Calendar.DATE, expiry);
//	        Date d = c.getTime();
	        
	        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	    	LocalDate localDate = LocalDate.now();
	    	String time=dtf.format(localDate);
	    	System.out.println(dtf.format(localDate));
	    	LocalDate tomorrow = localDate.plusDays(expiry); 
			
			userRepository.updateToken(user.getApiToken(), tomorrow, phone);

			Long id = userRepository.findId1(phone);
			statusObject.put("code", 200);
			statusObject.put("message", "successfull");
			contentObject.put("apiToken", user.getApiToken());
			contentObject.put("id", id);
			
			jsonObject.put("content", contentObject);
			jsonObject.put("status", statusObject);
			return jsonObject;
			
			
		}
		
		else{
			statusObject.put("code", 200);
			statusObject.put("message", "Please enter valid Otp ");
			
			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
		
		
	}
		else{
			statusObject.put("code", 200);
			statusObject.put("message", "Please enter valid Otp ");
			
			jsonObject.put("status", statusObject);
			return jsonObject;
	
			}
	}
	
//	@GetMapping("/api/getalluser")
//	public Page<User> getAllUsers(Pageable page)
//	{
//		
//        String headerToken = request.getHeader("apiToken");
//		
//		int verifyapiToken = userRepository.verifyUserApiToken(headerToken);
//		
//		if(verifyapiToken == 0) {
//			
//			String error = "UnAuthorised User";
//			String message = "Not Successful";
//			
//			throw new UnauthorisedException(401, error, message);
//		}
//		return userRepository.findAll(page);
//	}
	
	
}

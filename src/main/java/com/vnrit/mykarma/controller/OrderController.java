package com.vnrit.mykarma.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vnrit.mykarma.exception.UnauthorisedException;
import com.vnrit.mykarma.model.Items;
import com.vnrit.mykarma.model.Merchant;
import com.vnrit.mykarma.model.MerchantProfile;
import com.vnrit.mykarma.model.Order;
import com.vnrit.mykarma.model.Profile;
import com.vnrit.mykarma.model.User;
import com.vnrit.mykarma.repository.AdminRepository;
import com.vnrit.mykarma.repository.ItemsRepository;
import com.vnrit.mykarma.repository.MerchantProfileRepository;
import com.vnrit.mykarma.repository.MerchantRepository;
import com.vnrit.mykarma.repository.OrderRepository;
import com.vnrit.mykarma.repository.ProfileRepository;
import com.vnrit.mykarma.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class OrderController {
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired
	private ItemsRepository itemsRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private MerchantRepository merchantRepository;
	@Autowired
	private MerchantProfileRepository merchantProfileRepository;
	
	@GetMapping("/api/getorders/{user_id}")
	public List<Map> getOrdersByUserId(@PathVariable(value="user_id")Long userId,Order order) {
		
		String headerToken = request.getHeader("apiToken");
		
		long getUserId=profileRepository.getUserId(userId);
		
		String dbApiToken = userRepository.getDbApiToken(getUserId);
		
		if(!dbApiToken.equals(headerToken)) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";
		
		throw new UnauthorisedException(401, error, message);
	}
		return orderRepository.getOrderByUserId(userId);
	}
	
	@GetMapping("/api/merchantgetallorders/{merchantid}")
	public Page<List<Map>> getAllUserOrdersMerchant(@PathVariable(value="merchantid")Long merchantid,Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		long getmerchantId=merchantProfileRepository.getMerchantId(merchantid);
		
		String dbApiToken = merchantRepository.getdbApitoken(getmerchantId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		return orderRepository.getOrdersOfMerchants(merchantid, page);
	}
	
	
	@PostMapping("/api/postorder/{userprofileid}")
	public JSONObject postOrder(@PathVariable(value="userprofileid")Long userProfileId,@RequestParam(value="merchantId")Long merchantProfileId,@RequestParam(value="itemId")Long itemId,@RequestBody Order order) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long getUserId=profileRepository.getUserId(userProfileId);
		
		String dbApiToken = userRepository.getDbApiToken(getUserId);
		
		if(!dbApiToken.equals(headerToken)) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";
		
		throw new UnauthorisedException(401, error, message);
	}
		
		Items itemObj = itemsRepository.getOne(itemId);
		Long itemMerchantId=itemsRepository.getMerchantProfileId(itemId);
		Long itemQuantity=itemsRepository.getQuantity(itemId);
	
		order.setItemId(itemObj);
		order.setItemName(itemObj.getItemName());
		order.setDiscountPrice(itemObj.getDiscountPrice());

		MerchantProfile merchantObj = merchantProfileRepository.getOne(merchantProfileId);
//		System.out.println(order.getMerchantId());

		order.setMerchantProfileId(merchantObj);
		order.setHotelProfileName(merchantObj.getHotelName());
		Long quantity=Long.parseLong(order.getQuantity());
//		System.out.println(quantity);
if(merchantProfileId==itemMerchantId) {
	Profile profileObj = profileRepository.getOne(userProfileId);

	order.setUserProfileId(profileObj);
	if(itemQuantity >= quantity) {
		orderRepository.save(order);
		
		statusObject.put("code", 400);
		statusObject.put("message", "Order request is successfull");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	else {
		statusObject.put("code", 400);
		statusObject.put("message", "Quantity is more");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	
}
statusObject.put("code", 200);
statusObject.put("message", "Merchant Profile Id is Not Equal");

jsonObject.put("status", statusObject);

return jsonObject;
		
	}
	
	@PatchMapping("/api/orderapprovaltrue/{id}")
	public JSONObject updateOrderTrue(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long merchantProfileId=orderRepository.getOrderMerchantId(id);
		
		long merchantId=merchantProfileRepository.getMerchantId(merchantProfileId);
		
		String dbApiToken = merchantRepository.getdbApitoken(merchantId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		
		orderRepository.updateApprovedTrue(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Order approved true successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}
	
	@PatchMapping("/api/orderapprovalfalse/{id}")
	public JSONObject updateOrderFalse(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long merchantProfileId=orderRepository.getOrderMerchantId(id);
		
		long merchantId=merchantProfileRepository.getMerchantId(merchantProfileId);
		
		String dbApiToken = merchantRepository.getdbApitoken(merchantId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		
		orderRepository.updateApprovedFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Order approved false successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}
	
	@PatchMapping("/api/ordercompletedtrue/{id}")
	public JSONObject updateCompletedTrue(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long merchantProfileId=orderRepository.getOrderMerchantId(id);
		
		long merchantId=merchantProfileRepository.getMerchantId(merchantProfileId);
		
		String dbApiToken = merchantRepository.getdbApitoken(merchantId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		
		orderRepository.updateCompletedTrue(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Order completed true successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}
	
	@PatchMapping("/api/ordercompletedfalse/{id}")
	public JSONObject updateCompletedFalse(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long merchantProfileId=orderRepository.getOrderMerchantId(id);
		
		long merchantId=merchantProfileRepository.getMerchantId(merchantProfileId);
		
		String dbApiToken = merchantRepository.getdbApitoken(merchantId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		
		orderRepository.updateCompletedFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Order completed false successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}

	
	@DeleteMapping("/api/deleteorder/{id}")
	public JSONObject deleteOrder(@PathVariable(value="id")Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		orderRepository.deleteOrder(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Order deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@GetMapping("/api/admingetorders/{user_id}")
	public List<Map> getOrdersByUserIdToAdmin(@PathVariable(value="user_id")Long userId,Order order) {
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return orderRepository.getOrderByUserId(userId);
	}
	
	@GetMapping("/api/admingetallorders")
	public Page<Order> getAllOrders(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return orderRepository.findAll(page);
	}
	
	@GetMapping("/api/gettrueordersofmerchant/{merchantid}")
	public Page<List<Map>> getAllTrueOrders(@PathVariable(value="merchantid")Long merchantid,Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		long getmerchantId=merchantProfileRepository.getMerchantId(merchantid);
		
		String dbApiToken = merchantRepository.getdbApitoken(getmerchantId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		return orderRepository.getApprovedTrue(merchantid, page);
	}

	@GetMapping("/api/getfalseordersofmerchant/{merchantid}")
	public Page<List<Map>> getAllFalseOrders(@PathVariable(value="merchantid")Long merchantid,Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		long getmerchantId=merchantProfileRepository.getMerchantId(merchantid);
		
		String dbApiToken = merchantRepository.getdbApitoken(getmerchantId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		return orderRepository.getApprovedFalse(merchantid, page);
	}
	
	@GetMapping("/api/getcompletedfalseordersofmerchant/{merchantid}")
	public Page<List<Map>> getAllcompletedFalseOrders(@PathVariable(value="merchantid")Long merchantid,Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		long getmerchantId=merchantProfileRepository.getMerchantId(merchantid);
		
		String dbApiToken = merchantRepository.getdbApitoken(getmerchantId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		return orderRepository.getCompletedFalse(merchantid, page);
	}
	
	@GetMapping("/api/getcompletedtrueordersofmerchant/{merchantid}")
	public Page<List<Map>> getAllcompletedTrueOrders(@PathVariable(value="merchantid")Long merchantid,Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		long getmerchantId=merchantProfileRepository.getMerchantId(merchantid);
		
		String dbApiToken = merchantRepository.getdbApitoken(getmerchantId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		return orderRepository.getCompletedTrue(merchantid, page);
	}
	
	@GetMapping("/api/gettrueordersofuser/{user_id}")
	public Page<List<Map>> getApprovedTrueOfUser(@PathVariable(value="user_id")Long userId,Pageable page) {
		
		String headerToken = request.getHeader("apiToken");
		
		long getUserId=profileRepository.getUserId(userId);
		
		String dbApiToken = userRepository.getDbApiToken(getUserId);
		
		if(!dbApiToken.equals(headerToken)) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";
		
		throw new UnauthorisedException(401, error, message);
	}
		return orderRepository.getApprovedTrueOfUser(userId, page);
	}
	
	@GetMapping("/api/getfalseordersofuser/{user_id}")
	public Page<List<Map>> getApprovedFalseOfUser(@PathVariable(value="user_id")Long userId,Pageable page) {
		
		String headerToken = request.getHeader("apiToken");
		
		long getUserId=profileRepository.getUserId(userId);
		
		String dbApiToken = userRepository.getDbApiToken(getUserId);
		
		if(!dbApiToken.equals(headerToken)) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";
		
		throw new UnauthorisedException(401, error, message);
	}
		return orderRepository.getApprovedFalseOfUser(userId, page);
	}
	
	@GetMapping("/api/getcompletedtrueordersofuser/{user_id}")
	public Page<List<Map>> getCompletedTrueOfUser(@PathVariable(value="user_id")Long userId,Pageable page) {
		
		String headerToken = request.getHeader("apiToken");
		
		long getUserId=profileRepository.getUserId(userId);
		
		String dbApiToken = userRepository.getDbApiToken(getUserId);
		
		if(!dbApiToken.equals(headerToken)) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";
		
		throw new UnauthorisedException(401, error, message);
	}
		return orderRepository.getCompletedTrueOfUser(userId, page);
	}
	
	@GetMapping("/api/getcompletedfalseordersofuser/{user_id}")
	public Page<List<Map>> getCompletedFalseOfUser(@PathVariable(value="user_id")Long userId,Pageable page) {
		
		String headerToken = request.getHeader("apiToken");
		
		long getUserId=profileRepository.getUserId(userId);
		
		String dbApiToken = userRepository.getDbApiToken(getUserId);
		
		if(!dbApiToken.equals(headerToken)) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";
		
		throw new UnauthorisedException(401, error, message);
	}
		return orderRepository.getCompletedFalseOfUser(userId, page);
	}
	
	
	@GetMapping("/api/adminmerchantgetallorders/{merchantid}")
	public Page<List<Map>> getAllUserOrdersMerchantAdmin(@PathVariable(value="merchantid")Long merchantid,Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return orderRepository.getOrdersOfMerchants(merchantid, page);
	}




}

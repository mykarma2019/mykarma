package com.vnrit.mykarma.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vnrit.mykarma.exception.UnauthorisedException;
import com.vnrit.mykarma.model.Feedback;
import com.vnrit.mykarma.model.MerchantProfile;
import com.vnrit.mykarma.model.Order;
import com.vnrit.mykarma.model.Profile;
import com.vnrit.mykarma.repository.AdminRepository;
import com.vnrit.mykarma.repository.FeedbackRepository;
import com.vnrit.mykarma.repository.ItemsRepository;
import com.vnrit.mykarma.repository.MerchantProfileRepository;
import com.vnrit.mykarma.repository.MerchantRepository;
import com.vnrit.mykarma.repository.OrderRepository;
import com.vnrit.mykarma.repository.ProfileRepository;
import com.vnrit.mykarma.repository.UserRepository;

@CrossOrigin(origins = "*",allowedHeaders = "*")
@RestController
public class FeedbackController {
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired
	private FeedbackRepository feedbackRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private MerchantRepository merchantRepository;
	@Autowired
	private MerchantProfileRepository merchantProfileRepository;
	
	@PostMapping("/api/postrevivew/{userId}")
	public JSONObject postReview(@PathVariable("userId")Long userProfileId,@RequestParam(value="merchantProfileId")Long merchantProfileId,@RequestBody Feedback feedback) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long getUserId=profileRepository.getUserId(userProfileId);
		
		String dbApiToken = userRepository.getDbApiToken(getUserId);
		
		if(!dbApiToken.equals(headerToken)) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";
		
		throw new UnauthorisedException(401, error, message);
	}
		MerchantProfile merchantObj = merchantProfileRepository.getOne(merchantProfileId);

		feedback.setMerchantProfileId(merchantObj);
	
			Profile profileObj = profileRepository.getOne(userProfileId);

			feedback.setUserProfileId(profileObj);
				feedbackRepository.save(feedback);

				statusObject.put("code", 200);
				statusObject.put("message", "Review posted successfully");
				contentObject.put("review", feedback);

				jsonObject.put("status", contentObject);
				jsonObject.put("status", statusObject);

				return jsonObject;
	}
	
	@GetMapping("/api/admingetallreviews")
	public Page<Feedback> getAllFeedbacks(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return feedbackRepository.findAll(page);
	}
	
	@GetMapping("/api/getreviewsofmerchant/{merchantid}")
	public Page<List<Map>> getAllMerchantReviews(@PathVariable(value="merchantid")Long merchantid,Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		long getmerchantId=merchantProfileRepository.getMerchantId(merchantid);
		
		String dbApiToken = merchantRepository.getdbApitoken(getmerchantId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		return feedbackRepository.getReviewsOfMearchant(merchantid, page);
	}
	
	@GetMapping("/api/admingetreviewsofmerchant/{merchantid}")
	public Page<List<Map>> getAllMerchantReviewsAdmin(@PathVariable(value="merchantid")Long merchantid,Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return feedbackRepository.getReviewsOfMearchant(merchantid, page);
	}
	
	@GetMapping("/api/admingetreviewsofuser/{userProfileId}")
	public Page<List<Map>> getAllUserReviewsAdmin(@PathVariable(value="userProfileId")Long userProfileId,Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return feedbackRepository.getReviewsOfUser(userProfileId, page);
	}
	
	@GetMapping("/api/getreviewsofuser/{userProfileId}")
	public Page<List<Map>> getAllUserReviews(@PathVariable(value="userProfileId")Long userProfileId,Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		long getUserId=profileRepository.getUserId(userProfileId);
		
		String dbApiToken = userRepository.getDbApiToken(getUserId);
		
		if(!dbApiToken.equals(headerToken)) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";
		
		throw new UnauthorisedException(401, error, message);
	}
		return feedbackRepository.getReviewsOfUser(userProfileId, page);
	}
	
	@GetMapping("/api/gettruereviews")
	public Page<List<Map>> getAllTrueReviews(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return feedbackRepository.getApprovedTrue(page);
	}

	@GetMapping("/api/getfalsereviews")
	public Page<List<Map>> getAllFalseOrders(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return feedbackRepository.getApprovedFalse(page);
	}
	
	@PatchMapping("/api/reviewapprovaltrue/{id}")
	public JSONObject updateReviewTrue(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		
		
		feedbackRepository.updateApprovedTrue(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Review approved true successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}
	
	@PatchMapping("/api/reviewapprovalfalse/{id}")
	public JSONObject updateReviewFalse(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		
		
		feedbackRepository.updateApprovedFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Review approved false successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}
	
	@DeleteMapping("/api/deletereview/{id}")
	public JSONObject deleteReview(@PathVariable(value="id")Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		feedbackRepository.deleteReview(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Review deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}


}

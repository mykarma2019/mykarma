package com.vnrit.mykarma.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vnrit.mykarma.repository.AdminRepository;
import com.vnrit.mykarma.repository.MerchantProfileRepository;
import com.vnrit.mykarma.repository.MerchantRepository;
import com.vnrit.mykarma.repository.TypeOfBussinessRepository;
import com.vnrit.mykarma.exception.UnauthorisedException;
import com.vnrit.mykarma.model.Merchant;
import com.vnrit.mykarma.model.MerchantProfile;
import com.vnrit.mykarma.model.TypeOfBussiness;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class MerchantProfileController {
	
	@Autowired
	private MerchantProfileRepository merchantProfileRepository;
	
	@Value("${file.upload-dir}")
	private String uploadPath;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private MerchantRepository merchantRepository;
	
	@Autowired
	private TypeOfBussinessRepository typeOfBussinessRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	
	@PostMapping("/api/postmerchantdetails/{merchantId}")
	public JSONObject postMerchantProfileDetails(@PathVariable(value = "merchantId") Long merchantId,@RequestParam(value="TypeOfBussinessId") Long TypeOfBussinessId,@ModelAttribute MerchantProfile profile,@RequestParam MultipartFile file) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String dbApitoken = merchantRepository.getdbApitoken(merchantId);
		
		String headerToken = request.getHeader("apiToken");
		
       if(!dbApitoken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
       String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
       profile.setFileName(fileName);
       
       OutputStream opStream = null;
		
		byte[] byteContent;
		try {
			byteContent = file.getBytes();
			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is "+myFile);
			
			
			
			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {
				
				myFile.createNewFile();
				
			}
			
			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Merchant merchantObj = merchantRepository.getOne(merchantId);
		
		profile.setMerchantId(merchantObj);
		
		merchantProfileRepository.save(profile);
		statusObject.put("code", 200);
		statusObject.put("message", "successfully uploaded");
		jsonObject.put("status", statusObject);
		
		return jsonObject;
		
	}
	
	@PostMapping("/api/postmerchantdetailsbyadmin/{merchantId}")
	public JSONObject postMerchantProfileDetailsByAdmin(@PathVariable(value = "merchantId") Long merchantId,@RequestParam(value="TypeOfBussinessId") Long TypeOfBussinessId,@ModelAttribute MerchantProfile profile,@RequestParam MultipartFile file) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		 int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
       if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
       String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
       profile.setFileName(fileName);
       
       OutputStream opStream = null;
		
		byte[] byteContent;
		try {
			byteContent = file.getBytes();
			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is "+myFile);
			
			
			
			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {
				
				myFile.createNewFile();
				
			}
			
			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Merchant merchantObj = merchantRepository.getOne(merchantId);
		
		profile.setMerchantId(merchantObj);
		
		merchantProfileRepository.save(profile);
		statusObject.put("code", 200);
		statusObject.put("message", "successfully uploaded");
		jsonObject.put("status", statusObject);
		
		return jsonObject;
		
	}
	
	
	@GetMapping("/api/getallmerchantprofiledetails")
	public List<Map> getAllMerchantDetails() {
		
		
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
         if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
			
		}
		
		return merchantProfileRepository.getAllProfile();
		
		
	}
	
	@GetMapping("/api/getmerchantprofilebyid/{id}")
	public MerchantProfile getMerchantProfileById(@PathVariable(value = "id") Long id) {
		
     String headerToken = request.getHeader("apiToken");
		
		Long merchantId = merchantProfileRepository.getMerchantId(id);
		String getdbApitoken = merchantRepository.getdbApitoken(merchantId);
		 if(!getdbApitoken.equals(headerToken)) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		 
		return merchantProfileRepository.getMerchantProfileById(id);
		
	}
	
	@GetMapping("/api/getmerchantprofilebyidadmin/{id}")
	public MerchantProfile getMerchantProfileByIdAdmin(@PathVariable(value = "id") Long id) {
		
     String headerToken = request.getHeader("apiToken");
		
     int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
     if(verifyapiToken == 0) {
		
		String error = "UnAuthorised User";
		String message = "Not Successful";

		throw new UnauthorisedException(401, error, message);
		
	}
		 
		return merchantProfileRepository.getMerchantProfileById(id);
		
	}
	
	@PatchMapping("api/updatemerchantprofiledetails/{id}")
	public JSONObject updateMerchantProfileDetails(@PathVariable(value = "id") Long id,@RequestBody MerchantProfile input,@RequestParam Long typeOfBussinessId) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
           String headerToken = request.getHeader("apiToken");
           Long merchantId = merchantProfileRepository.getMerchantId(id);
		
           String dbApitoken = merchantRepository.getdbApitoken(merchantId);
		 if(!dbApitoken.equals(headerToken)) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		 
		 MerchantProfile one = merchantProfileRepository.getOne(id);
		 
		 TypeOfBussiness typeOfBussinessObj = typeOfBussinessRepository.getOne(typeOfBussinessId);
		 
		 String address = input.getAddress();
		 Boolean approved = input.getApproved();
		 String category = input.getCategory();
		 String city = input.getCity();
		 String state = input.getState();
		 String contactPerson = input.getContactPerson();
		 String description = input.getDescription();
		 String hotelName = input.getHotelName();
//		 TypeOfBussiness typeOfBussinessId = input.getTypeOfBussinessId();
		
		 
		 if(address==null) {
			 address=one.getAddress();
		 }
		 if(approved==null) {
			 approved=one.getApproved();
		 }
		 if(category==null) {
			 category=one.getCategory();
		 }
		 if(city==null) {
			 city=one.getCity();
		 }
		 if(state==null) {
			 state=one.getState();
		 }
		 if(contactPerson==null) {
			 contactPerson=one.getContactPerson();
		 }
		 if(description==null) {
			 description=one.getDescription();
		 }
		 if(hotelName==null) {
			 hotelName=one.getHotelName();
		 }
		 if(typeOfBussinessId!=null) {
			 one.setTypeOfBussinessId(typeOfBussinessObj);
		 }
		
		 
		 merchantProfileRepository.updateMerchantProfileDetails(address, approved, category, city, state, contactPerson, description, hotelName,  id);
		 statusObject.put("code", 200);
			statusObject.put("message", "updated successfull");
		
			jsonObject.put("status", statusObject);
			return  jsonObject;
		 
		 
		
	}
	
	@PatchMapping("api/updatemerchantprofiledetailsbyadmin/{id}")
	public JSONObject updateMerchantProfileDetailsByAdmin(@PathVariable(value = "id") Long id,@RequestBody MerchantProfile input,@RequestParam Long typeOfBussinessId) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
           String headerToken = request.getHeader("apiToken");
           int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
           if(verifyapiToken == 0) {
      		
      		String error = "UnAuthorised User";
      		String message = "Not Successful";

      		throw new UnauthorisedException(401, error, message);
      		
      	}
		 
		 MerchantProfile one = merchantProfileRepository.getOne(id);
		 TypeOfBussiness typeOfBussinessObj = typeOfBussinessRepository.getOne(typeOfBussinessId);
		 
		 String address = input.getAddress();
		 Boolean approved = input.getApproved();
		 String category = input.getCategory();
		 String city = input.getCity();
		 String state = input.getState();
		 String contactPerson = input.getContactPerson();
		 String description = input.getDescription();
		 String hotelName = input.getHotelName();
		
		 
		 if(address==null) {
			 address=one.getAddress();
		 }
		 if(approved==null) {
			 approved=one.getApproved();
		 }
		 if(category==null) {
			 category=one.getCategory();
		 }
		 if(city==null) {
			 city=one.getCity();
		 }
		 if(state==null) {
			 state=one.getState();
		 }
		 if(contactPerson==null) {
			 contactPerson=one.getContactPerson();
		 }
		 if(description==null) {
			 description=one.getDescription();
		 }
		 if(hotelName==null) {
			 hotelName=one.getHotelName();
		 }
		 if(typeOfBussinessId!=null) {
			 one.setTypeOfBussinessId(typeOfBussinessObj);
		 }
		 
		
		 
		 merchantProfileRepository.updateMerchantProfileDetails(address, approved, category, city, state, contactPerson, description, hotelName,  id);
		 statusObject.put("code", 200);
			statusObject.put("message", "updated successfull");
		
			jsonObject.put("status", statusObject);
			return  jsonObject;
		 
		 
		
	}
	
	@GetMapping("/api/getallmerchantprofiledetailsstatustrue/{status}")
	public List<Map> getAllStatusTrue(@PathVariable(value = "status") Boolean status){
		
		String headerToken = request.getHeader("apiToken");
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return merchantProfileRepository.getAllStatusTrue(status);
	}
	
	@GetMapping("/api/getallmerchantprofiledetailsstatusfalse/{status}")
	public List<Map> getAllStatusFalse(@PathVariable(value = "status") Boolean status){
		
		String headerToken = request.getHeader("apiToken");
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		return merchantProfileRepository.getAllStatusFalse(status);
	}
	
	@DeleteMapping("/api/deletemerchantprofiledetails/{id}")
	public JSONObject deleteMerchantDetails(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		Long merchantId = merchantProfileRepository.getMerchantId(id);
		 String dbApitoken = merchantRepository.getdbApitoken(merchantId);
		 
		 
		 if(!dbApitoken.equals(headerToken)) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		 merchantProfileRepository.deleteMerchantProfile(id);
		 statusObject.put("code", 200);
			statusObject.put("message", "successfully deleted");
			
			jsonObject.put("status", statusObject);
			
			return jsonObject;
		
	}
	
	@DeleteMapping("/api/deletemerchantprofiledetailsbyadmin/{id}")
	public JSONObject deleteMerchantDetailsByAdmin(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		int verifyApiToken = adminRepository.verifyapiTokens(headerToken);
		 if(verifyApiToken==0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
				
			}
		 merchantProfileRepository.deleteMerchantProfile(id);
		 statusObject.put("code", 200);
			statusObject.put("message", "successfully deleted");
			
			jsonObject.put("status", statusObject);
			
			return jsonObject;
		
	}

}

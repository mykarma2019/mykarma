package com.vnrit.mykarma.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Items")
public class Items extends AuditModel{
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@Column(updatable = true)
	private String itemName;
	
	@Column(updatable = true)
	private String itemType;
	
	@Column(updatable = true)
	private int quantity;
	
	@Column(updatable = true)
	private String fileName;
	
	@Column(updatable = true)
	private int originalPrice;
	
	@Column(updatable = true)
	private int discountPrice;
	
	
	@Column
	private String buyBefore;
	
	
	@Column
	private String pickupBefore;
	
	@Column(updatable = true)
	private float rating;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "profileId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private MerchantProfile profileId;

	

	public String getBuyBefore() {
		return buyBefore;
	}

	public void setBuyBefore(String buyBefore) {
		this.buyBefore = buyBefore;
	}

	public String getPickupBefore() {
		return pickupBefore;
	}

	public void setPickupBefore(String pickupBefore) {
		this.pickupBefore = pickupBefore;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	

	public MerchantProfile getProfileId() {
		return profileId;
	}

	public void setProfileId(MerchantProfile profileId) {
		this.profileId = profileId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(int originalPrice) {
		this.originalPrice = originalPrice;
	}

	public int getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(int discountPrice) {
		this.discountPrice = discountPrice;
	}

	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
